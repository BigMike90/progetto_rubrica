Il file Target.txt è un file d'esempio, lo script scansiona quel file e crea un secondo file senza duplicati (Result.txt).

* Creare una cartella ed inserire all'interno Run.py ed il file desiderato (Target)
* Target.txt è solo un file di esempio con i numeri inseriti, sostituire con la tua rubrica e rinominare in Target.txt
* Aspettare che lo script finisca l'esecuzione ed aprire il file Result.txt